# AltjiraOS

[AltjiraOS](https://altjira.xyz) is an open Linux-based operating system designed from the ground-up
for the metaverse. This is a derivative of the excellent [carbonOS](https://carbon.sh).

## Project structure

- `elements/`: [BuildStream](https://buildstream.build) elements that define the OS's components
    - `pkgs/`: Defines all of the individual packages that make up the OS
    - `buildsystems/`: Convenient way to import all the dependencies for a standard build system (meson, autotools, cmake, etc)
    - `groups/`: The basic package groups that altjiraOS is split into. Everything that is pulled into an image is in a group
    - `images/`: Target device image definitions
    - `kernels/`: Variants of the Linux kernel supported by altjiraOS
    - `variants/`: Variants of the base OS
    - `junctions/`: Contains the definitions of all the junctions used in carbonOS (bootstrap, plugin sources, etc)
    - `tools/`: Elements that are there to help the build in some way and shouldn't be treated normally
- `files/`: Various auxiliary files that are part of altjiraOS's build (i.e. default config)
- `include/`: YAML fragments that are reused in `elements/` and `project.conf`
- `keys/`: Standard location to put GPG keys for signing altjiraOS. Ignored by git. See `keys/README.md` for more info
- `patches/`: Patches that get applied onto packages in `elements/pkgs/`. Ideally kept to a minimum
- `plugins/`: Custom BuildStream plugins that are used in `elements/`
    - `ostree*`: A buildstream plugin that exports files into an OSTree Repository. Written for [gnome-build-meta](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/tree/master/plugins)
- `project.conf`: The BuildStream project configuration
- `*.refs`: Used by BuildStream to keep track of the versions of various components
- `pub/`: Staging directory for publishing. Ignored by git
- `result/`: Standard location that `tools/checkout` exports BuildStream artifacts to
- `tools/`: Helpful scripts for building altjiraOS
    - `bumpver`: Increments altjiraOS's version number
    - `checkout`: Retrieves compiled artifacts from BuildStream and puts them into `result/`
    - `update`: Comprehensive script that updates every single package in the repo. RUN WITH CARE
    - `test`: Simple way to stage a few elements together and run them in a shell
    - `setup-devrepo`: Create the structure in pub/ for a development OSTree repo
    - `publish`: Publish os/base/x86_64/desktop and os/kernel/x86_64/generic to the dev repo in pub

## Build instructions

#### Dependencies

First, you need to install build dependencies. If you are running altjiraOS, this
is simple:
```bash
$ updatectl switch --base=devel
[Enter administrator password at the prompt]
[Reboot]
```

Your system should now be set up for altjiraOS and GDE development. If
you are not running altjiraOS, you'll need to install these packages to
compile the system:

- buildstream 1.6.x
- buildstream-external 0.27.2
- ostree (recent)
- cargo (recent)
- go 1.14+

#### Deciding on your target

altjiraOS is built somewhat modularly, and you need to decide which modules you
want to build. To do so, you'll need to answer a few questions:

> What device are you targeting? (`IMAGE`)

Device image definitions live in `elements/images/`, and they include device-specific
packages and configuration. Every image type in that folder has a README file that
will give you a good idea what devices that image is appropriate for, as well as
information that will help you answer the next few questions.

> What kernel do you want to use? (`KERNEL`)

Kernel definitions live in `elements/kernels`. Refer to your target device's README
file for guidance on what kernel to pick

> What architecture are you targetting? (`ARCH`)

This really depends on your needs. Your target device's README file will list which
CPU architectures are supported by that image.

> What variant of altjiraOS do you want to use? (`VARIANT`)

altjiraOS has a few basic variants that can be selected depending on your needs (desktop, devel, mobile).
They differ in available packages and in configuration.

#### Building

Now you can start by building the core OS. The core makes up most of the system;
it is only missing device-specific packages, a kernel, and an initramfs. To build the core, run:

```bash
$ bst build variants/VARIANT/files.bst
[...]
```

This will take a few hours, or longer depending on your hardware. Next, you'll
need to build a kernel+initramfs package. This makes the core you just built
bootable. Simply run:

```bash
$ bst --max-jobs N build kernels/KERNEL/repo.bst
[...]
```

Replace N with the number of CPU threads you want to allocate to this build.
Since it is only building a kernel, it is better to allocate all of the CPU to
one package instead of letting multiple packages build at once as we did with
the core. To find out how many threads your CPU has available, run `nproc`.

Next, you'll build a release image of the OS that you can flash onto an install
medium or directly onto the device, depending on your needs (and on the device's
properties. Please see `elements/images/DEVICE/README.md` for details about the
generated files and where they should go). Simply run:

```bash
$ bst build images/DEVICE/image.bst
[...]
$ tools/checkout images/DEVICE/image.bst
[...]
$ ls result/
altjiraOS-2022.5-DEVICE.img
```

#### Publishing

[TODO: Finish this with uploading the files to an ostree remote]

#### Debuginfo

Debuginfo gets special treatment in altjiraOS, because it is an extemely large but seldom-used
resource that is, at times, still necessary. Thus, we'd like to avoid distributing debuginfo
in any image. altjiraOS uses [debuginfod](https://sourceware.org/elfutils/Debuginfod.html) to
serve debuginfo to debuggers on-demand, which allows both a debuginfo-free OS and rich interactive
debugging. To build altjiraOS's debuginfo bundle, run:

```bash
$ bst build images/debug/image.bst
[...]
$ tools/checkout images/debug/image.bst
[...]
$ ls result/
altjiraOS-2022.5-debug.tar.zst
```

I'll leave configuring `debuginfod` to consume this bundle as an excercise to the reader. Once you have
`debuginfod` set up, just put the domain of your server (i.e. `https://debuginfod.carbon.sh/`) into
`/etc/debuginfod/your-company.urls` and altjiraOS will start querying your server for debuginfo

#### Building everything

If you are redistributing altjiraOS and want to build all available cores,
kernels, and images, you can do so with this command:

```bash
$ bst build tools/everything.bst
```

Then, you can publish these binaries with:

```bash
TODO
```
