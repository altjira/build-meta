#!/usr/bin/env bash

[ -d pub/repo ] || {
    echo "Make sure to run tools/setup-devrepo before running this"
    exit 1
}

[ -d keys/private ] || {
    echo "Make sure to run keys/init before running this"
    exit 1
}

# Unlock the private key
keys/unlock
GPGHOME=keys/private/unlocked
GPGKEY=`gpg --home=$GPGHOME -k --with-colons | sed '/^fpr:/q;d' | cut -d: -f10`

function pull-local() {
    # Pull the generated repo out of buildstream
    rm -r pub/staging || true
    bst checkout --hardlinks -d none --no-integrate $1 pub/staging || return

    # Pull contents into the repo
    PARENT=$(ostree --repo=pub/repo rev-parse $2 || echo none)
    ostree --repo=pub/repo pull-local pub/staging $2
    BASE=$(ostree --repo=pub/repo rev-parse $2)

    # Bail if nothing changed
    [ "$PARENT" == "$BASE" ] && return

    # Generate a new commit w/ proper metadata
    # This fixes the timestamp, parent, and ostree.collection-binding
    tools/_publish_gen_commit $PARENT $BASE $2

    # Sign the new commit
    ostree --repo=pub/repo gpg-sign --gpg-homedir=$GPGHOME $2 $GPGKEY

    # Generate static deltas
    [ "$PARENT" != "none" ] && ostree --repo=pub/repo static-delta generate --if-not-exists $2
}

pull-local variants/desktop/repo.bst os/base/x86_64/desktop
pull-local variants/devel/repo.bst os/base/x86_64/devel
pull-local kernels/mainline/repo.bst os/kernel/x86_64/mainline

ostree --repo=pub/repo prune --refs-only --depth=5
ostree --repo=pub/repo summary -u --gpg-sign=$GPGKEY --gpg-homedir=$GPGHOME

# Lock the private key
keys/lock
