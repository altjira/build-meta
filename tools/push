#!/usr/bin/env bash
# Expected environment variables:
# CARBON_PUSH_HOST = user@host.name, such that rsync and scp can access it
# CARBON_PUSH_REPO = /path/to/ostree/repo, on the remote
# CARBON_PUSH_DEBUG = /path/to/debug, on the remote
# CARBON_PUSH_IMG = /path/to/img, on the remote

function die {
    echo "ERROR! Please set $1 in your environment. See comment at the top of this script for more detail" 1>&2
    exit 1
}

[ -z "$CARBON_PUSH_HOST" ] && die "CARBON_PUSH_HOST";
[ -z "$CARBON_PUSH_REPO" ] && die "CARBON_PUSH_REPO";
[ -z "$CARBON_PUSH_DEBUG" ] && die "CARBON_PUSH_DEBUG";
[ -z "$CARBON_PUSH_IMG" ] && die "CARBON_PUSH_IMG";

echo "Pushing to remote $CARBON_PUSH_HOST"

# Push the ostree repo
echo "Pushing repo to $CARBON_PUSH_REPO"
tools/rsync-repos.py --src=pub/repo --dest $CARBON_PUSH_HOST:$CARBON_PUSH_REPO

# Push the debuginfo
echo "Pushing debuginfo to $CARBON_PUSH_DEBUG"
tools/checkout images/debug/image.bst
scp result/* $CARBON_PUSH_HOST:$CARBON_PUSH_DEBUG

# Push the ISO
echo "Pushing images to $CARBON_PUSH_IMG"
tools/checkout images/installer/image.bst
scp result/* $CARBON_PUSH_HOST:$CARBON_PUSH_IMG
