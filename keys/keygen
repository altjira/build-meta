#!/usr/bin/env bash
# Generate the private keys used to sign altjiraOS releases
export SUDO="${SUDO-sudo}"
GPGKEY=`gpg -k --with-colons | sed '/^fpr:/q;d' | cut -d: -f10`

cd keys
mkdir private &>/dev/null || true

# Create the LUKS box around the key
truncate --size 50MB private/next.loop
LUKSPASS="$(gpg --gen-random --armor 0 50)"
echo $LUKSPASS | cryptsetup luksFormat private/next.loop
echo $LUKSPASS | gpg --encrypt -r $GPGKEY -o private/next.pass -

# Unlock the LUKS box
mkdir private/unlocked
echo $LUKSPASS | $SUDO cryptsetup luksOpen private/next.loop altjiraOS-gpgkey
$SUDO mkfs.ext4 /dev/mapper/altjiraOS-gpgkey
$SUDO mount /dev/mapper/altjiraOS-gpgkey private/unlocked
$SUDO chown $UID private/unlocked
chmod 700 private/unlocked

# Create the key
export GNUPGHOME=private/unlocked
cat <<EOF | gpg --batch --generate-key -
Key-Type: RSA
Key-Length: 4096
Subkey-Type: RSA
Subkey-Length: 4096
Name-Real: altjiraOS
Expire-Date: 0
%no-protection
%commit
EOF
gpg --export -o private/next.pub

# Lock the LUKS box
$SUDO umount private/unlocked
$SUDO cryptsetup close altjiraOS-gpgkey
rmdir private/unlocked
