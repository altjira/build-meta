# altjiraOS - Keys
This directory contains the GPG keys used to sign
altjiraOS releases and the scripts to manage them.

`keys/private` contains the private keys used to
sign the binaries produced for altjiraOS. These files
are never committed to git and should be provided by
whoever is building. Ensure you have GPG set up for
encryption/decryption, then run `keys/init`.

altjiraOS keeps two keys around at the same time. There's
the "current" key which is being used to sign current
releases, and then there's the "next" key which will become
the new "current" at the time of rotation.

Private keys are stored in LUKS-encrypted loopback files.
`keys/unlock` will use your GPG keys to decrypt the password
for the LUKS volume and unlock the current private key.
`keys/lock` will re-lock this volume. Note that `tools/publish`
automatically unlocks/relocks the private key, so I'd recommend
taking your system offline when you run `tools/publish` to avoid
any risk of leaking a private key

To rotate the keys in preparation for a future release,
run `keys/rotate`. This will throw out your previous backup
key, back up the current key, make the "next" key the "current"
key, and then generate a new "next" key. Make sure to upload the
keys also!
