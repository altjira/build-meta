"""
BuildStream does not save file permissions and ownership.
include/excludes with integration commands is so complex that only
the "compose" plugin does it correctly. Because "compose" does not
save file permissions and loses integration commands (because they
are executed), that means we need to save it another file permissions
another way. This is where collect-initial-scripts works around the
issue. It provides a way to have integration scripts that we execute
when we pack into an image (filesystem, tar, ostree, etc.)

This plugin is also useful to execute integration commands only
before an image is created, to avoid executing these commands
needlessly and repeatedly during the build. For instance,
there's no need to build the fontconfig cache, or update the
desktop database, or etc more than once
"""

# Originally written for Freedesktop SDK:
# https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/blob/8a50ecc0/plugins/elements/collect_initial_scripts.py

import os
import re
from buildstream import Element, ElementError, Scope

class ExtractInitialScriptsElement(Element):
    def configure(self, node):
        self.node_validate(node, [ 'path' ])
        self.path = self.node_subst_member(node, 'path', '/initial_scripts')

    def preflight(self):
        runtime_deps = list(self.dependencies(Scope.RUN, recurse=False))
        if runtime_deps:
            raise ElementError("{}: Only build type dependencies supported by collect-integration elements".format(self))

        sources = list(self.sources())
        if sources:
            raise ElementError("{}: collect-integration elements may not have sources".format(self))

    def get_unique_key(self):
        return { 'path': self.path }

    def configure_sandbox(self, sandbox):
        pass

    def stage(self, sandbox):
        pass

    def assemble(self, sandbox):
        basedir = sandbox.get_directory()
        path = os.path.join(basedir, self.path.lstrip(os.sep))
        index = 0
        for dependency in self.dependencies(Scope.BUILD):
            public = dependency.get_public_data('initial-script')
            if public and 'script' in public:
                script = self.node_subst_member(public, 'script')
                index += 1
                depname = re.sub('[^A-Za-z0-9]', '_', dependency.name)
                basename = '{:03}-{}'.format(index, depname)
                filename = os.path.join(path, basename)
                os.makedirs(path, exist_ok=True)
                with open(filename, 'w') as f:
                    f.write(script)
                os.chmod(filename, 0o755)

        return os.sep

def setup():
    return ExtractInitialScriptsElement
