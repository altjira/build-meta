# mainline Linux kernel

This is the stock Linux kernel, using the mainline sources.

This kernel follows the `linux-rolling-stable` release, so
generally it'll follow `5.x` until `5.(x+2)`'s merge window
closes, at which point it'll start following `5.(x+1)`
