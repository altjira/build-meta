This is the debug image for altjiraOS. It packages up `/usr/lib/debug` into a tarball suitable for consumption by `debuginfod`.

- Kernel: N/A
- Variant: `devel`
- Arch: all
- Ships with installer: N/A
- Output file: `altjiraOS-VERSION-debug.tar.zst`
- How to install: N/A
