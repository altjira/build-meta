kind: stack
description: The graphical environment of carbonOS

depends:
# GNOME Shell
- pkgs/gnome-session.bst
- pkgs/gdm.bst
- pkgs/gnome-shell.bst
- pkgs/xdg-desktop-portal/gnome.bst

# Assets
- pkgs/fonts/all.bst
- pkgs/icons/adwaita.bst
- pkgs/icons/hicolor.bst
- pkgs/sound-theme-fdo.bst
- pkgs/gnome-backgrounds.bst
- pkgs/elementary-backgrounds.bst
- pkgs/gnome-user-docs.bst

# Pre-installed Applications
- pkgs/apps/nautilus.bst          # File browser
- pkgs/sushi.bst                  # Nautilus' previewer
- pkgs/apps/epiphany.bst          # Web browser
- pkgs/apps/text-editor.bst       # Text Editor
- pkgs/apps/gnome-calculator.bst  # Calculator
#- pkgs/apps/camera.bst            # Camera
- pkgs/apps/seahorse.bst          # Keyring/Password manager
- pkgs/apps/gnome-contacts.bst    # Contacts
- pkgs/apps/geary.bst             # E-Mail client
- pkgs/apps/gnome-calendar.bst    # Calendar
- pkgs/apps/eog.bst               # Image viewer
- pkgs/apps/evince.bst            # Document viewer
#- pkgs/apps/gnome-music.bst       # Music player
#- pkgs/apps/gnome-videos.bst      # Video player
- pkgs/apps/gnome-clocks.bst      # World Clocks / Alarms / Timers
- pkgs/apps/gnome-software.bst    # App Store
- pkgs/apps/gnome-weather.bst     # Weather app
- pkgs/apps/gnome-terminal.bst    # Terminal
- pkgs/apps/system-monitor.bst    # Task Manager
- pkgs/apps/gnome-disks.bst       # Drive manager
- pkgs/apps/gnome-logs.bst        # Log viewer
- pkgs/apps/baobab.bst            # Disk usage
#- pkgs/apps/gnome-fonts.bst       # Font viewer/installer
- pkgs/apps/gnome-characters.bst  # Character map
- pkgs/apps/gnome-settings.bst    # Settings
- pkgs/yelp.bst                   # Help browser
- pkgs/apps/gnome-tour.bst        # Tour of GNOME Shell
- pkgs/apps/extension-manager.bst # App to install/manage GNOME extensions

# Accessibility
- pkgs/orca.bst                  # Screen reader

# Codecs & VA-API
- pkgs/gstreamer/plugins/all.bst # Gstreamer plugins
- pkgs/intel-vaapi/all.bst       # Intel VA-API drivers

# Misc
- pkgs/carbon-setup.bst          # Graphical installer & initial setup
- pkgs/plymouth.bst              # Boot animation daemon
- pkgs/ibus/all.bst              # Input methods
- pkgs/xdg-user-dirs-gtk.bst     # Common home directories (Documents, Music, etc)
- pkgs/desktop-file-utils.bst    # Desktop entry db management
#- pkgs/source-thumbnailer.bst    # Thumbnailer for common source code files
- pkgs/wl-clipboard.bst          # Helpful command-line utility to interact with clipboard
- pkgs/webp-pixbuf-loader.bst    # Webp support for gdk-pixbuf

# Nvidia userspace driver
- pkgs/nvidia/userspace.bst
