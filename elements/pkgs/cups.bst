kind: autotools

sources:
- kind: git_tag
  url: github:OpenPrinting/cups
  exclude:
  - '*b*'
  - '*rc*'
- kind: local
  path: files/cups
  directory: carbon

depends:
- pkgs/systemd/all.bst
- pkgs/gnutls.bst
- pkgs/avahi.bst
- pkgs/libusb.bst
- pkgs/dbus.bst
- pkgs/acl.bst
- pkgs/libpng.bst
- pkgs/libtiff.bst
- pkgs/krb5.bst
- pkgs/colord.bst
- pkgs/libpaper.bst

build-depends:
- buildsystems/autotools.bst

variables:
  license-files: 'LICENSE NOTICE'
  conf-local: >-
    --with-dbusdir=%{datadir}/dbus-1
    --enable-pam=yes
    --enable-systemd
    --enable-raw-printing
    --enable-ssl=yes
    --enable-threads
    --enable-gnutls
    --enable-libusb
    --enable-avahi
    --enable-libpaper
    --with-cups-group=lp
    --with-system-groups=wheel
    --with-rundir=/run/cups
    localedir=%{datadir}/locale
    DSOFLAGS="$CFLAGS $LDFLAGS"

config:
  install-commands:
    (>):
    - | # Install carbonOS configuration files
      cd carbon
      install -Dm644 tmpfiles.conf %{install-root}%{libdir}/tmpfiles.d/cups.conf
      install -Dm644 sysusers.conf %{install-root}%{libdir}/sysusers.d/cups.conf
      install -Dm644 cups.pam %{install-root}%{libdir}/pam.d/cups
    - | # Don't put anything into /run
      rm -r %{install-root}/run
