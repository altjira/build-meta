kind: make

sources:
- kind: git_tag
  url: github:libreswan/libreswan
  track: main

# TODO: Remove if issue is resolved upstream
# https://github.com/libreswan/libreswan/issues/725
# manual-updates
- kind: local
  path: files/ipsec.tmpfiles
  directory: carbon

depends:
- pkgs/systemd/all.bst
- pkgs/python/all.bst
- pkgs/nss.bst
- pkgs/libevent.bst
- pkgs/curl.bst
- pkgs/openldap.bst
- pkgs/libseccomp.bst
- pkgs/libcap-ng.bst
- pkgs/pam.bst

build-depends:
- pkgs/flex.bst
- buildsystems/autotools.bst

variables:
  make-args: >-
    LINUX_VARIANT=carbon
    FINALLIBEXECDIR=%{libexecdir}/ipsec
    FINALSBINDIR=%{sbindir}
    FINALMANDIR=%{mandir}
    FINALSYSCONFDIR=%{sysconfdir}
    INITSYSTEM=systemd
    PYTHON_BINARY=%{bindir}/python3
    SHELL_BINARY=%{bindir}/bash
    USE_LDAP=true
    USE_SECCOMP=true
    USE_LIBCAP_NG=true
    USE_DNSSEC=false

config:
  configure-commands:
  - sed -i '/$(XMLTO) man/s/.*/\ttouch $(foreach refname,$(call refnames,$<), $(builddir)\/$(refname))/' mk/manpages.mk

  install-commands:
    (>):
    - install -Dm644 carbon/ipsec.tmpfiles %{install-root}%{libdir}/tmpfiles.d/ipsec.conf
    - install -Dm644 packaging/fedora/libreswan-sysctl.conf %{install-root}%{libdir}/sysctl.d/50-libreswan.conf
    - mv %{install-root}%{sysconfdir}/pam.d %{install-root}%{libdir}/pam.d
    - rm -rv %{install-root}%{sysconfdir}/logrotate.d
